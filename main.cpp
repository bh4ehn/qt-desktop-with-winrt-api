
// add progma comment lib to tell linker to link WinRT library
#pragma comment(lib, "windowsapp")
#include <QApplication>
#include <QPushButton>
#include <QMessageBox>
// must include <winrt/base.h>
#include <winrt/base.h>
// include every component
// some component is implicitly used, will throw errors on compile if not included
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.Foundation.Metadata.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.ApplicationModel.Appointments.h>

// using namespace for shorter type names
using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::ApplicationModel::Appointments;

IAsyncAction ProcessAppointmentAsync() {
    try {
        // DO NOT use Windows::ApplicationModel::Appointments::AppointmentManager.ShowAddAppointmentAsync
        // this require CoreWindow and not support in desktop applications
        // and definitely throws exception

        hstring calendarName = L"Qt";

        // find previously created calender by name
        AppointmentStore appointmentStore{ co_await AppointmentManager::RequestStoreAsync(AppointmentStoreAccessType::AllCalendarsReadWrite) };
        IVectorView<AppointmentCalendar> calendars{ co_await appointmentStore.FindAppointmentCalendarsAsync(FindAppointmentCalendarsOptions::IncludeHidden) };

        bool isFoundCalendar = false;
        AppointmentCalendar appointmentCalendar = nullptr;

        for (AppointmentCalendar const calendar: calendars) {
            if (calendar.DisplayName() == calendarName) {
                isFoundCalendar = true;
                appointmentCalendar = calendar;
                break;
            }
        }

        // if not created ever, create one
        if (!isFoundCalendar) {
            appointmentCalendar = co_await appointmentStore.CreateAppointmentCalendarAsync(calendarName);
        }

        // may cache the AppointmentCalendar instance in global
        // or store the calendar id in application configuration, then use AppointmentStore::GetAppointmentCalendarAsync to retrieve

        // new Appointment, fill the properties with actual data
        Appointment appointment{};
        appointment.Subject(L"TEST Subject");
        appointment.StartTime(clock::now());
        appointment.Duration(std::chrono::hours{2});
        appointment.Details(L"TEST details");

        // save appointment, then check Calendar app
        co_await appointmentCalendar.SaveAppointmentAsync(appointment);

    } catch (winrt::hresult_error const& e) {
        // error handling is not necessary, just for dev usage
        auto hResult = e.code().value;
        auto message = e.message();
        QMessageBox::critical(nullptr, "", QString("hResult: %1, Message: %2").arg(QString::number(hResult), message), QMessageBox::Ok);
    }
}

int main(int argc, char *argv[]) {
    // initialize Windows Runtime in this thread
    winrt::init_apartment();

    // common Qt GUI
    QApplication a(argc, argv);
    a.setApplicationName("Appointments");
    QPushButton button("Hello world!", nullptr);

    QObject::connect(&button, &QPushButton::clicked,[&] {
        // this case use co_await for not blocking the main thread
        ProcessAppointmentAsync().get();
    });
    button.resize(200, 100);
    button.show();
    return QApplication::exec();
}